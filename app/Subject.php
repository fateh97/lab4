<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function lecturer()
    {
        return $this->belongsTo(Lecturer::class);
    }
}
